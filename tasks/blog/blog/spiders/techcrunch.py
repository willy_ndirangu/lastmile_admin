# -*- coding: utf-8 -*-
import scrapy
from ..items import BlogItem


class TechcrunchSpider(scrapy.Spider):
    name = "techcrunch"
    allowed_domains = ["techcrunch.com"]
    start_urls = (
        'https://techcrunch.com/popular/',
    )

    def parse(self, response):
        blogs = [response.xpath("//div[@class='block block-thumb ']/div[@class='block-content']/p[@class='excerpt']")]
        print(len(blogs[0]))
        for i in range(len(blogs[0])):
            item = BlogItem()
            item["title"] = response.xpath(
                "//div[@class='block block-thumb ']/div[@class='block-content']/h2[@class='post-title']/a/text()").extract()[
                i]
            item["link"] = response.xpath(
                "//div[@class='block block-thumb ']/div[@class='block-content']/h2[@class='post-title']/a/@href").extract()[
                i]
            item["body"] = response.xpath(
                "//div[@class='block block-thumb ']/div[@class='block-content']/p[@class='excerpt']/text()").extract()[
                i]
            try:
                item['categories'] = response.xpath(
                    "//div[@class='block block-thumb ']/div[@class='tags']/a[@class='tag']/span/text()").extract()[i]
            except IndexError:
                item['categories'] = "Unknown"
            yield item
