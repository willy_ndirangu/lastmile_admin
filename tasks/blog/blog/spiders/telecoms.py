# -*- coding: utf-8 -*-
import scrapy
from ..items import BlogItem


class TelecomsSpider(scrapy.Spider):
    name = "blog"
    allowed_domains = ["telecomstechnews.com"]
    start_urls = (
        'http://www.telecomstechnews.com/news/',
    )

    def parse(self, response):
        articles = [response.xpath(
            "//article/div[@class='image_and_summary_wrapper']/div[@class='summary']/p[2]")]
        print(len(articles[0]))
        for i in range(len(articles[0])):
            item = BlogItem()
            item["title"] = response.xpath(
                "//div[@id='hub_content_wrapper']/div[@id='hub_content']/section[@class='news']/article/a/h2/text()").extract()[
                i]
            item["link"] = "http://www.telecomstechnews.com" + \
                           response.xpath("//div[@id='hub_content']/section[@class='news']/article/a/@href").extract()[
                               i]
            item['categories'] = response.xpath("//h4/span[@id='categories']/a[@id='categories']/text()").extract()[i]

            try:
                item["body"] = response.xpath(
                    "//div[@id='hub_content']/section[@class='news']/article/div[@class='image_and_summary_wrapper']/div[@class='summary']/p[2]/text()").extract()[
                    i]
                yield item
            except IndexError:
                item['body'] = "Click the link to read more"
