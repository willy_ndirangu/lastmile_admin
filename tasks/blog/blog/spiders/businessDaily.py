# -*- coding: utf-8 -*-
import scrapy
from ..items import BlogItem


class TechcrunchSpider(scrapy.Spider):
    name = "bd"
    allowed_domains = ["businessdailyafrica.com"]
    start_urls = (
        'http://www.businessdailyafrica.com/Corporate-News/539550-539550-x5t3id/index.html',
    )

    def parse_urls(self, response):
        item = BlogItem()
        item["link"] = response.meta['link']
        item["title"] = response.xpath(
            "//article[@class='article-story page-box']/div[@class='page-box-inner']/header/h1/text()").extract()[0]
        item["body"] = response.xpath(
            "//article[@class='article-story page-box']/div[@class='page-box-inner']/div[3]/p/text()").extract()[0]

        try:
            item['categories'] = "Kenya" + "," + response.xpath(
                "//article[@class='article-story page-box']/div[@class='page-box-inner']/header/h4/a/text()").extract()[
                0]
        except IndexError:
            item['categories'] = "Unknown"
        yield item

    def parse(self, response):
        urls = response.xpath(
            "//div[@class='content-half'][2]/div[@class='columns']/article[@class='article article-list-regular']/a/@href").extract()
        item = BlogItem()
        for url in urls:
            if url:
                yield scrapy.Request(url, meta={'item': item, 'link': url}, callback=self.parse_urls)
