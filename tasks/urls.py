from django.conf.urls import url
from tasks import views

urlpatterns = [
    url(r'^dashboard/blog$', views.BlogListView.as_view(), name='blogs'),
    url(r'^dashboard/blog/read/(?P<user_id>\d+)/(?P<blog_id>\d+)$', views.read_blog, name='blogs_read'),
    url(r'^dashboard/query/news/(?P<query>[a-zA-Z\s]*)$', views.query_news, name='query_news'),
]
