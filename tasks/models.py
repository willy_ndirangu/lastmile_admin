from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify
from dashboard.models import Employee, Subscriber
from django.core.urlresolvers import reverse

from django.db.models.signals import post_save
from django.core.mail import EmailMessage, send_mass_mail, EmailMultiAlternatives
from django.template.loader import render_to_string


# Create your models here.
class Blog(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    link = models.CharField(max_length=1000)
    slug = models.CharField(max_length=255, unique=True)
    body = models.TextField()
    categories = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.slug

    class Meta:
        db_table = 'blog'


def pre_save_blog(sender, instance, *args, **kwargs):
    if instance.pk is None:
        slug = slugify(instance.title)
        instance.slug = slug


pre_save.connect(pre_save_blog, sender=Blog)


class BlogRead(models.Model):
    id = models.AutoField(primary_key=True)
    read_by = models.ForeignKey(Employee, models.DO_NOTHING, null=True)
    blog = models.ForeignKey('Blog', models.DO_NOTHING, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = 'user_read_blog'


class Email(models.Model):
    id = models.AutoField(primary_key=True)
    subject = models.CharField(max_length=1000)
    to = models.CharField(max_length=1000)
    from_email = models.EmailField(null=True, max_length=255)
    body = models.TextField()
    attachment = models.FileField(blank=True, upload_to='attachments/documents/')
    sent_by = models.ForeignKey(Employee, models.DO_NOTHING, null=True, related_name="sender")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.id

    def get_absolute_url(self):
        return reverse("emails-sent")

    class Meta:
        db_table = 'emails'


def do_send_email(sender, instance, created, **kwargs):
    if created:
        html_content = render_to_string('mails/dashboard_system_email.html',
                                        {'body': instance.body})
        subject, from_email = (instance.subject, instance.from_email)
        subscribers = Subscriber.objects.all()
        if instance.from_email != 'sales@lastmile-networks.net':
            if instance.to == "All":
                subscriber_to_send = [subscriber.email for subscriber in subscribers]
            else:
                subscriber_to_send = [subscriber.email for subscriber in subscribers if
                                      Subscriber.objects.filter(location__location=instance.to)]
        else:
            subscriber_to_send = [instance.to]
        msg = EmailMultiAlternatives(subject, html_content, instance.from_email, subscriber_to_send,
                                     bcc=["team@lastmile-networks.net"])
        msg.content_subtype = "html"
        if instance.attachment:
            msg.attach_file(instance.attachment.path)
        msg.send()


post_save.connect(do_send_email, sender=Email)


