from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.generic.list import ListView
from tasks.models import Blog, BlogRead
from dashboard.models import Employee
from dashboard.mixins import LoginRequiredMixin
from django.db.models import Q
from django.core import serializers
from django.contrib.auth.decorators import login_required


class BlogListView(LoginRequiredMixin, ListView):
    model = Blog
    queryset = Blog.objects.order_by('-created_at')
    paginate_by = 9

    def get_queryset(self, **kwargs):
        queryset = super(BlogListView, self).get_queryset(**kwargs)
        # Get the q GET parameter
        query = self.request.GET.get('q')
        if query:
            queryset = Blog.objects.order_by('-created_at').filter(Q(body__icontains=query) |
                                                                   Q(title__icontains=query) |
                                                                   Q(categories__icontains=query)
                                                                   )

        return queryset


def read_blog(request, user_id, blog_id):
    """insert information about read articles """
    BlogRead.objects.create(read_by=Employee.objects.get(pk=user_id), blog=Blog.objects.get(pk=blog_id))
    return HttpResponse("")


@login_required
def query_news(request, query):
    queryset = Blog.objects.order_by('-created_at').filter(Q(body__icontains=query) |
                                                           Q(title__icontains=query) |
                                                           Q(categories__icontains=query)
                                                           )
    return HttpResponse(serializers.serialize('json', queryset[:10]))
