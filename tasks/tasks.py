from celery.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from django.core.mail import EmailMessage, send_mass_mail, EmailMultiAlternatives
from dashboard.models import Subscriber, Employee, Payment, Tickets, IncomeStatement
from django.utils.timezone import now, timedelta
from django.template.loader import render_to_string
from django.conf import settings
from dashboard.financial_statements import income_statement
from datetime import datetime

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(hour=8, minute=30)),
    name="send_email_five_days_before_disconnection",
    ignore_result=True
)
def send_email_five_days_before_disconnection():
    """
    Sends a subscriber email five or three days before disconnection
    """
    subscribers = Subscriber.objects.all()
    subscribers_to_disconnect = [subscriber for subscriber in subscribers if
                                 (subscriber.connection_end_date.date() - timedelta(days=5)) == now().date() or (
                                     subscriber.connection_end_date.date() - timedelta(days=3)) == now().date()]
    for subscriber in subscribers_to_disconnect:
        html_content = render_to_string('mails/disconnection_reminder.html',
                                        {'disconnection_date': subscriber.connection_end_date,
                                         'name': subscriber.first_name})
        subject, from_email, to = ('Service Notice', 'clientservice@lastmile-networks.net', subscriber.email)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to], bcc=["info@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(hour=12, minute=30)),
    name="notify_account_to_terminate",
    ignore_result=True
)
def notify_account_to_terminate():
    """
    Notify admin of pending disconnections
    """
    subscribers = Subscriber.objects.all()
    subscribers_to_disconnect = [subscriber for subscriber in subscribers if
                                 (subscriber.connection_end_date.date()) == now().date()]
    if subscribers_to_disconnect:
        employees = Employee.objects.all()
        employee_emails = [employee.email for employee in employees]

        html_content = render_to_string('mails/service_termination_reminder.html',
                                        {'subscribers': subscribers_to_disconnect})
        subject, from_email = ('Disconnect Notice', 'support@lastmile-networks.net')
        msg = EmailMultiAlternatives(subject, html_content, from_email, employee_emails,
                                     bcc=["info@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(hour=7, minute=30)),
    name="send_email_on_disconnection",
    ignore_result=True
)
def send_on_disconnection():
    """
    Sends a subscriber email on day of   disconnection
    """
    subscribers = Subscriber.objects.all()
    subscribers_to_disconnect = [subscriber for subscriber in subscribers if
                                 (subscriber.connection_end_date.date()) == now().date()]
    for subscriber in subscribers_to_disconnect:
        html_content = render_to_string('mails/disconnected_notify.html',
                                        {'disconnection_date': subscriber.connection_end_date,
                                         'name': subscriber.first_name})
        subject, from_email, to = ('Disconnection Notice', 'clientservice@lastmile-networks.net', subscriber.email)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to], bcc=["info@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(hour=1, minute=10)),
    name="send_task_email_on_due_date",
    ignore_result=True
)
def send_email_on_task_due_date():
    """
    Sends a subscriber email on day of   disconnection
    """
    tasks = Tickets.objects.all()
    tasks_to_do = [task for task in tasks if
                   (task.due_date.date()) == now().date()]
    if tasks_to_do:
        employee_emails = [employee.email for employee in Employee.objects.all()]
        html_content = render_to_string('mails/tasks_to_do_notify.html',
                                        {'tasks': tasks_to_do})
        subject, from_email, to = ('Tasks To Do', 'clientservice@lastmile-networks.net', employee_emails)
        msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=["info@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(hour=3, minute=30)),
    name="send_disconnected_email",
    ignore_result=True
)
def send_disconnected_email():
    """
        Sends a subscriber email on day of   disconnection
        """
    subscribers = Subscriber.objects.all()
    subscribers_to_disconnect = [subscriber for subscriber in subscribers if
                                 (subscriber.connection_end_date.date() + timedelta(days=2)) == now().date()]
    for subscriber in subscribers_to_disconnect:
        html_content = render_to_string('mails/disconnected.html',
                                        {'disconnection_date': subscriber.connection_end_date,
                                         'name': subscriber.first_name})
        subject, from_email, to = ('Disconnected', 'clientservice@lastmile-networks.net', subscriber.email)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to], bcc=['team@lastmile-networks.net'])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(hour=3, minute=10)),
    name="send_employee_login_email",
    ignore_result=True
)
def send_employee_login_email():
    """
        Sends a subscriber email on day of   disconnection
        """
    employees = [employee for employee in Employee.objects.all() if
                 employee.last_login is not None and (employee.last_login.date() + timedelta(days=365)) < now().date()]
    for employee in employees:
        html_content = render_to_string('mails/employee_login.html', {'employee': employee})
        subject, from_email, to = (
            'Why dont you use our own products', 'system@lastmile-networks.net', employee.email)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to], bcc=['wndirangu@lastmile-networks.net'])
        msg.content_subtype = "html"
        msg.send()


@periodic_task(
    run_every=(crontab(0, 0, day_of_month='1')),
    name="prepare_monthly_income_statement",
    ignore_result=True
)
def prepare_monthly_income_statement():
    """
        Sends a subscriber email on day of   disconnection
        """
    filename = income_statement.gen_statement_pdf()
    current_month_name = datetime.strftime(datetime.now()-timedelta(days=5), '%B')
    period = datetime.strftime(datetime.now()-timedelta(days=5), '%B') + " " + str((datetime.now()-timedelta(days=5)).year)
    IncomeStatement.objects.create(period=period, filename=filename)
    employees = [employee for employee in Employee.objects.all()]
    for employee in employees:
        html_content = render_to_string('mails/income_stmt.html', {'employee': employee, 'month': current_month_name})
        subject, from_email, to = (
            'Income statement for the month of ' + current_month_name, 'system@lastmile-networks.net', employee.email)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to], bcc=['wndirangu@lastmile-networks.net'])
        msg.content_subtype = "html"
        msg.attach_file(settings.MEDIA_ROOT + '/financial-statements/' + filename + '.pdf')
        msg.send()
