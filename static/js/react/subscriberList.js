var React = require('react');

var List = React.createClass({

    render: function () {
        return (
            <li ><a target="_blank" href={this.props.result.fields.link}>{this.props.result.fields.title}</a></li>
        );
    }

});

module.exports = List;