$(document).foundation()
var app = angular.module('app', ['ngSanitize'])
app.config(['$interpolateProvider', '$httpProvider', function ($interpolateProvider, $httpProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

}]);


app.controller('DashboardHomeController', ['$scope', '$http', function ($scope, $http) {

    $http.get('https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1').then(function (response) {
        $scope.pending = response.data;


    });
}]);


app.controller('BlogContoller', ['$scope', '$http', function ($scope, $http) {

    $scope.readBlog = function ($user, $blog) {
        $http.get('/dashboard/blog/read/' + $user + '/' + $blog).then(function (response) {

        })
    }
}]);

app.controller('RequestsController', ['$scope', '$http', function ($scope, $http) {

    $http.get('https://admin.lastmile-networks.net/api/requests?format=json').then(function (response) {
        $scope.requests = response.data;


    });
}]);

app.controller('ReportsController', ['$scope', '$http', function ($scope, $http) {

    $http.get('https://admin.lastmile-networks.net/dashboard/reports/subscriber/all').then(function (response) {
        $scope.subscribers = response.data;


    });
}]);


app.controller('FinancialStatementController', ['$scope', '$http', function ($scope, $http) {
    $scope.data;
    $http.get('https://admin.lastmile-networks.net/api/payment_data/create?q=json').then(function (response) {
        $scope.data = response.data;
        amount = []
        Object.keys($scope.data).forEach(function (key) {
            amount.push($scope.data[key])
        });
        var data = {
            labels: Object.keys($scope.data),
            datasets: [{
                data: amount,
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)"
            }]
        };

        var ctx = document.getElementById("LineWithLine").getContext("2d");

        Chart.types.Line.extend({
            name: "LineWithLine",
            draw: function () {
                Chart.types.Line.prototype.draw.apply(this, arguments);

                var point = this.datasets[0].points[this.options.lineAtIndex]
                var scale = this.scale

                // draw line
                // this.chart.ctx.beginPath();
                // this.chart.ctx.moveTo(point.x, scale.startPoint + 24);
                // this.chart.ctx.strokeStyle = '#ff0000';
                // this.chart.ctx.lineTo(point.x, scale.endPoint);
                // this.chart.ctx.stroke();
                // var x = this.scale.xScalePaddingLeft * 0.4;
                var ctx = this.chart.ctx;
                ctx.save();
                // text alignment and color
                ctx.textAlign = "center";
                ctx.textBaseline = "bottom";
                ctx.fillStyle = this.options.scaleFontColor;
                // position
                var x = this.scale.xScalePaddingLeft * 1.5;
                var y = this.chart.height / 2;
                // change origin
                ctx.translate(x, y)
                // rotate text
                ctx.rotate(-90 * Math.PI / 180);
                ctx.fillText("Amount in Kshs", 0, 0);
                ctx.restore();


            }
        });

        new Chart(ctx).LineWithLine(data, {
            datasetFill: true,
            lineAtIndex: 2
        });


    });


}]);

/**
 * All day datepicker
 */
app.directive('allPicker', function () {
        return {
            restrict: 'A',
            require: '^?ngModel',
            link: function (scope, element, attrs, ngModel) {
                $(element).fdatepicker({
                    format: 'yyyy-mm-dd'
                })
            }
        }

    }
);


app.controller('InvoiceCtrl', ['$scope', '$http', '$window', function ($scope, $http, $window) {
    $scope.choices = [];
    $scope.titles = [];
    $scope.total = 0
    $scope.quote_no = ''


    $scope.addNewChoice = function () {
        var newItemNo = $scope.choices.length + 1;
        $scope.choices.push({'id': 'choice' + newItemNo});


    };

    $scope.removeChoice = function () {
        var lastItem = $scope.choices.length - 1;
        $scope.choices.splice(lastItem);
    };
    $scope.removeChoiceSelected = function ($id) {
        $scope.choices.splice($id, 1);
    };

    $scope.removeTitle = function () {
        var lastItem = $scope.titles.length - 1;
        $scope.titles.splice(lastItem);
    };

    $scope.addNewTitle = function () {
        var newItemNo = $scope.titles.length + 1;
        $scope.titles.push({'id': 'title' + newItemNo});


    };
    $scope.getTotal = function () {
        var total = 0;
        for (var i = 0; i < $scope.choices.length; i++) {

            total += ($scope.choices[i].unit_price * $scope.choices[i].quantity);
        }
        return total;
    }
    $scope.SubmitQuote = function () {
        console.log($scope.choices)
        url = '/api/invoice/create'
        $http.post(url, $scope.choices).success(function (data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
            // console.log(data);
            $window.location.href = '/dashboard/invoice/' + $scope.quote_no + '/preview';


        }).error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an err
        });

    };

}]);

app.controller('InvoiceCopyCtrl', ['$scope', '$http', '$window', function ($scope, $http, $window) {
    /**
     * variable declaration
     * choices=> data with the customer containing all quotes
     * total=> contains information for vat and total value
     * quote_no=> the quote number of the quote to be copied
     * newCustomer => the new Customer to Create
     * */
    $scope.choices = [];
    $scope.total = 0
    $scope.quote_no = '';
    $scope.newCustomer = {}


    $scope.$watch('quote_id', function () {
        //When your value is set, you can retrieve it
        copy_from_url = '/api/quotation/' + $scope.quote_id + '/quote';
        $http.get(copy_from_url).then(function (response) {
            $scope.choices = response.data;

        });
    });


    $scope.addNewChoice = function () {
        var newItemNo = $scope.choices.quote_customer.length + 1;
        $scope.choices.quote_customer.push({'id': 'choice' + newItemNo});


    };

    $scope.removeChoice = function () {
        var lastItem = $scope.choices.quote_customer.length - 1;
        $scope.choices.quote_customer.splice(lastItem);
    };
    $scope.removeChoiceSelected = function ($id) {
        $scope.choices.splice($id, 1);
    };

    $scope.removeTitle = function () {
        var lastItem = $scope.titles.length - 1;
        $scope.titles.splice(lastItem);
    };

    $scope.addNewTitle = function () {
        var newItemNo = $scope.titles.length + 1;
        $scope.titles.push({'id': 'title' + newItemNo});


    };
    $scope.getTotal = function () {
        var total = 0;
        for (var i = 0; i < $scope.choices.quote_customer.length; i++) {

            total += ($scope.choices.quote_customer[i].unit_price * $scope.choices.quote_customer[i].quantity);
        }
        return total;
    }


    $scope.SubmitCopyQuote = function () {
        $scope.newCustomer.Customer = $scope.choices.Customer;
        $scope.newCustomer.title = $scope.choices.title;

        //url to create quote customer
        url = '/api/quotation/create'
        $http.post(url, $scope.newCustomer).success(function (data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
            $scope.quotationCreated = data;
            // $window.location.href = '/dashboard/invoice/' + $scope.quote_no + '/preview';
            for (var i = 0; i < $scope.choices.quote_customer.length; i++) {

                $scope.choices.quote_customer[i].customer = $scope.quotationCreated.id;
            }
            $http.post('/api/invoice/create', $scope.choices.quote_customer).success(function (data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available

                $window.location.href = '/dashboard/invoice/' + $scope.quotationCreated.quote_no + '/preview';


            }).error(function (data, status, headers, config) {
                $scope.errors = data;
            });
        }).error(function (data, status, headers, config) {
            $scope.errors = data;
        });

    };

}])
;