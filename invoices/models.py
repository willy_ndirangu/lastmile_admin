from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from dashboard.models import Employee, Subscriber
from django.core.urlresolvers import reverse


# Create your models here.
class Quotation(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    Customer = models.CharField(max_length=255)
    quote_no = models.CharField(max_length=45, blank=True, null=True, unique=True)
    customer_id = models.CharField(max_length=45, blank=True, null=True, unique=True)
    updated_by = models.ForeignKey('dashboard.Employee', models.DO_NOTHING, null=True,
                                   related_name="updated_invoice_record")
    created_by = models.ForeignKey('dashboard.Employee', models.DO_NOTHING, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.quote_no

    class Meta:
        db_table = 'quotation'

    def get_absolute_url(self):
        return reverse("quotation-detail", kwargs={"pk": self.id})


def pre_save_quotation(sender, instance, *args, **kwargs):
    if instance.pk is None:
        now = timezone.now()
        if Quotation.objects.all().count() == 0:
            quotation_id = 1
        else:
            quotation_id = Quotation.objects.latest('id').id + 1
        quote_no = "LMN/0" + str(int(now.year) % 100) + "/" + str(now.month) + "/" + str(quotation_id)
        customer_id = "LMN/" + str(now.year) + "/" + str(quotation_id)
        instance.quote_no = quote_no
        instance.customer_id = customer_id


pre_save.connect(pre_save_quotation, sender=Quotation)


class QuoteDescription(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=255)
    title = models.CharField(max_length=255, null=True)
    quantity = models.IntegerField(db_column='quantity', blank=True, null=True)
    unit_price = models.IntegerField(db_column='unit_price', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    due_date = models.DateTimeField(null=True)
    customer = models.ForeignKey('Quotation', models.DO_NOTHING, null=False, default=1, related_name='quote_customer')

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("quote_description", kwargs={"pk": self.id})

    class Meta:
        db_table = 'quote_description'
