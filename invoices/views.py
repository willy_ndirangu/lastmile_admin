from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from invoices.models import Quotation, QuoteDescription
from invoices.forms import QuotationForm, QuotationDescriptionForm
from dashboard.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils.timezone import now
import weasyprint
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse_lazy
from datetime import datetime


class QuotationListView(LoginRequiredMixin, ListView):
    model = Quotation
    queryset = Quotation.objects.all().order_by('-id')
    paginate_by = 9


class QuotationDetailView(LoginRequiredMixin, DetailView):
    model = Quotation
    slug_field = 'quote_no'


class QuotationCreateView(LoginRequiredMixin, CreateView):
    model = Quotation
    form_class = QuotationForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(QuotationCreateView, self).form_valid(form)


class QuotationUpdateView(LoginRequiredMixin, UpdateView):
    model = Quotation
    slug_field = 'quote_no'
    success_url = reverse_lazy('quote-list')

    form_class = QuotationForm

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(QuotationUpdateView, self).form_valid(form)


# Create your views here.

class QuotationTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'invoices/create.html'

    def get_context_data(self, **kwargs):
        context = super(QuotationTemplateView, self).get_context_data(**kwargs)
        context['quote'] = Quotation.objects.get(pk=self.kwargs['pk'])
        return context


class QuotationPreView(LoginRequiredMixin, DetailView):
    model = Quotation
    slug_field = 'quote_no'
    template_name = 'invoices/quote_preview.html'


class QuotationDescriptionDetailView(LoginRequiredMixin, DetailView):
    model = QuoteDescription


class QuotationDescriptionDeleteView(LoginRequiredMixin, DeleteView):
    model = QuoteDescription

    def get_success_url(self):
        kwargs = {'slug': self.object.customer.quote_no}
        return reverse_lazy('quote-details', kwargs=kwargs)


class QuotationDescriptionUpdateView(LoginRequiredMixin, UpdateView):
    model = QuoteDescription

    form_class = QuotationDescriptionForm

    def get_success_url(self):
        kwargs = {'slug': self.object.customer.quote_no}
        return reverse_lazy('quote-details', kwargs=kwargs)


# copy a quote

class QuotationCopyTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'invoices/copy.html'

    def get_context_data(self, **kwargs):
        context = super(QuotationCopyTemplateView, self).get_context_data(**kwargs)
        context['quote'] = Quotation.objects.get(pk=self.kwargs['pk'])
        return context


@login_required
def quotation_pdf(request, quote_no):
    quotation = Quotation.objects.get(quote_no=quote_no)

    html = render_to_string('pdfs/quotation.html',
                            {'object': quotation})
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=\
            "{}_.pdf"'.format(quote_no)
    weasyprint.HTML(string=html).write_pdf(response,
                                           stylesheets=[weasyprint.CSS(
                                               settings.STATIC_ROOT + '/css/quote_pdf.css'), weasyprint.CSS(
                                               settings.STATIC_ROOT + '/css/app.css')])
    return response


@login_required
def proforma_pdf(request, quote_no):
    quotation = Quotation.objects.get(quote_no=quote_no)

    html = render_to_string('pdfs/proforma.html',
                            {'object': quotation, 'today': datetime.now()
                             })
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=\
            "{}_.pdf"'.format(quote_no)
    weasyprint.HTML(string=html).write_pdf(response,
                                           stylesheets=[weasyprint.CSS(
                                               settings.STATIC_ROOT + '/css/quote_pdf.css'), weasyprint.CSS(
                                               settings.STATIC_ROOT + '/css/app.css')])
    return response
