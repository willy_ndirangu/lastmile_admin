from django.conf.urls import url
from invoices import views

urlpatterns = [

    url(r'^dashboard/invoices/$', views.QuotationCreateView.as_view(), name='invoices'),
    url(r'^dashboard/(?P<pk>[-\w]+)/invoices/$', views.QuotationTemplateView.as_view(), name='quotation-detail'),

    url(r'^dashboard/invoices/all/$', views.QuotationListView.as_view(),
        name='quote-list'),
    url(r'dashboard/invoices/detail/(?P<slug>.+)/$',
        views.QuotationDetailView.as_view(), name='quote-details'),
    url(r'dashboard/invoice/(?P<slug>.+)/update/$',
        views.QuotationUpdateView.as_view(),
        name='quote-update'),

    url(r'dashboard/invoice/(?P<slug>.+)/preview/$',
        views.QuotationPreView.as_view(),
        name='invoice-preview'),

    url(r'dashboard/invoice/(?P<quote_no>.+)/pdf/$',
        views.quotation_pdf,
        name='invoice-pdf'),
    url(r'dashboard/proforma/invoice/(?P<quote_no>.+)/pdf/$',
        views.proforma_pdf,
        name='proforma-pdf'),

    # Quote Description urls
    url(r'^dashboard/invoices/item/(?P<pk>[-\w]+)/edit/$', views.QuotationDescriptionUpdateView.as_view(),
        name='quote-item-edit'),
    url(r'^dashboard/invoices/item/(?P<pk>[-\w]+)/details/$', views.QuotationDescriptionDetailView.as_view(),
        name='quote-item-details'),
    url(r'^dashboard/invoices/item/(?P<pk>[-\w]+)/delete/$', views.QuotationDescriptionDeleteView.as_view(),
        name='quote-item-delete'),

    # Copy a quote

    url(r'^dashboard/invoices/quote/(?P<pk>[-\w]+)/copy/$', views.QuotationCopyTemplateView.as_view(),
        name='quote-copy'),


]
