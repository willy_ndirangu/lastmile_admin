from invoices.models import Quotation,QuoteDescription
from django import forms


class QuotationForm(forms.ModelForm):
    """
    A form that creates a user given an email and password
    """
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = Quotation
        exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'quote_no', 'customer_id']


class QuotationDescriptionForm(forms.ModelForm):
    """
    A form that creates a user given an email and password
    """
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = QuoteDescription
        exclude = ['created_at', 'updated_at', 'customer', 'due_date']
