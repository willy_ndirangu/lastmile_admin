from api.models import InternetServiceRequest
from rest_framework.generics import ListAPIView, CreateAPIView
from api.serializers.serializers import RequestSerializer, MailgunSerializer, InvoiceDescriptionSerializer, \
    QuotationSerializer, QuotationRetrieveSerializer
from api.api_tasks.post_save import ApiEmail
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets
from dashboard.models import Subscriber, InternetPackageDetail, Payment, OtherPayments, Expenses
from dashboard.views.expenses import get_by_month_amount
from collections import Counter, OrderedDict
from datetime import datetime
from dashboard.mixins import LoginRequiredMixin
from rest_framework.mixins import RetrieveModelMixin
from rest_framework import generics
from invoices.models import QuoteDescription, Quotation


# Create your views here.
class RequestListApiView(ListAPIView):
    queryset = InternetServiceRequest.objects.order_by('-id')
    serializer_class = RequestSerializer


class RequestCreateApiView(CreateAPIView):
    queryset = InternetServiceRequest.objects.all()
    serializer_class = RequestSerializer

    def perform_create(self, serializer):
        serializer.save()
        ApiEmail.send_email_on_request(self, **self.request.data)
        ApiEmail.notify_user_of_request(self, **self.request.data)


class MailgunCreateApiView(CreateAPIView):
    serializer_class = MailgunSerializer

    def perform_create(self, serializer):
        serializer.save()


# Api view
class FinancialDataView(LoginRequiredMixin, APIView):
    """
    A view that returns the count of active users in JSON.
    """
    renderer_classes = (JSONRenderer,)

    def get(self, request, format=None):
        subscriber_payments = get_by_month_amount(Payment, 'payment_date', 'amount')
        other_payments = get_by_month_amount(OtherPayments, 'payment_date', 'amount')

        data = {}
        data2 = {}
        for payment in other_payments:
            data.update({datetime.strftime((payment['month']), '%B'): payment['amount__sum']})
        for payment in subscriber_payments:
            data2.update({datetime.strftime((payment['month']), '%B'): payment['amount__sum']})

        first = Counter(data)
        second = Counter(data2)

        return Response(OrderedDict(sorted((first + second).items(), reverse=True)))


""" quote Description Create"""


class QuoteDescriptionViewSet(LoginRequiredMixin, CreateAPIView):
    """This view provides list, detail, create, retrieve, update
    and destroy actions for Things."""
    model = QuoteDescription
    serializer_class = InvoiceDescriptionSerializer

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]

            # check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super(QuoteDescriptionViewSet, self).get_serializer(*args, **kwargs)


class QuotationCreateApiView(LoginRequiredMixin, CreateAPIView):
    serializer_class = QuotationSerializer

    def perform_create(self, serializer):

        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class QuotationRetrieveApiView(LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Quotation.objects.all()
    serializer_class = QuotationRetrieveSerializer
