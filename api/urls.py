from django.conf.urls import url, include
from rest_framework import routers
from api.views import (
    RequestListApiView,
    RequestCreateApiView,
    MailgunCreateApiView,
    FinancialDataView,
    QuoteDescriptionViewSet,
    QuotationCreateApiView,
    QuotationRetrieveApiView
)

urlpatterns = [
    url(r'^api/requests$', RequestListApiView.as_view(), name='list_requests'),
    url(r'^api/request/create$', RequestCreateApiView.as_view(), name='create_requests'),
    url(r'^api/mailgun/create$', MailgunCreateApiView.as_view(), name='mailgun_view'),
    url(r'^api/payment_data/create$', FinancialDataView.as_view(), name='mailgun_view'),
    url(r'^api/invoice/create$', QuoteDescriptionViewSet.as_view(), name='create_invoice_quote'),
    url(r'^api/quotation/create$', QuotationCreateApiView.as_view(), name='create_quotation_quote'),
    url(r'^api/quotation/(?P<pk>[0-9]+)/quote$', QuotationRetrieveApiView.as_view(), name='get_quotation_quote'),

]
