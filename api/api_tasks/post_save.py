from django.core.mail import EmailMessage, send_mass_mail, EmailMultiAlternatives
from django.template.loader import render_to_string


class ApiEmail:
    def send_email_on_request(self, email, name, location, phone_number):
        html_content = render_to_string('mails/request_notify.html',
                                        {'name': name, 'email': email, 'location': location,
                                         'phone_number': phone_number})
        subject, from_email, to = ('New request', 'clientservice@lastmile-networks.net', ['team@lastmile-networks.net'])
        msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=["info@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()

    def notify_user_of_request(self, email, name, location, phone_number):
        html_content = render_to_string('mails/request_notify_user.html',
                                        {'name': name, 'email': email, 'location': location,
                                         'phone_number': phone_number})
        subject, from_email, to = ('Internet Service request', 'clientservice@lastmile-networks.net', [email])
        msg = EmailMultiAlternatives(subject, html_content, from_email, to, bcc=["support@lastmile-networks.net"])
        msg.content_subtype = "html"
        msg.send()


