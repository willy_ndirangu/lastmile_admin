from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from api.models import InternetServiceRequest
from dashboard.models import Subscriber, InternetPackageDetail, Payment, OtherPayments, Expenses
from invoices.models import Quotation, QuoteDescription


class RequestSerializer(ModelSerializer):
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = InternetServiceRequest
        fields = ['name', 'location', 'email', 'phone_number', 'created_at']


class InternetPackageSerializer(ModelSerializer):
    internet_package_type = serializers.StringRelatedField()

    class Meta:
        model = InternetPackageDetail


class SubscriberSerializer(ModelSerializer):
    created_at = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", required=False, read_only=True)
    location = serializers.StringRelatedField()
    internet_package = InternetPackageSerializer()

    class Meta:
        model = Subscriber


class MailgunSerializer(serializers.Serializer):
    event = serializers.CharField()
    recipient = serializers.CharField()

    def save(self):
        recipient = self.validated_data['recipient']
        event = self.validated_data['event']


class QuotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotation
        fields = ('id', 'Customer', 'title','quote_no')


class InvoiceDescriptionSerializer(serializers.ModelSerializer):
    customer = serializers.PrimaryKeyRelatedField(queryset=Quotation.objects.all())

    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(InvoiceDescriptionSerializer, self).__init__(many=many, *args, **kwargs)

    class Meta:
        model = QuoteDescription
        fields = ('customer', 'title', 'description', 'unit_price', 'quantity')


class QuotationRetrieveSerializer(serializers.ModelSerializer):
    quote_customer = InvoiceDescriptionSerializer(many=True, read_only=True)

    class Meta:
        model = Quotation
