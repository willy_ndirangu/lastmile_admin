# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-09-27 15:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='internetservicerequest',
            name='location',
            field=models.CharField(max_length=1000),
        ),
    ]
