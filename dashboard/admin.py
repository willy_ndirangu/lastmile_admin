from django.contrib import admin
from .models import Employee, EmployeeLevel, Subscriber

# Register your models here.
admin.site.register(EmployeeLevel)
admin.site.register(Employee)
admin.site.register(Subscriber)
