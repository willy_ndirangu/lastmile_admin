var React = require('react');
var axios = require('axios');
import List from './List';

var SearchAddForm = React.createClass({
    getInitialState: ()=> {

        return {
            search: '',
            results: []
        }


    },
    search: function (e) {
        this.setState({search: e.target.value});

        this.OnCharChange(this.state.search);

    },
    OnCharChange: function (text) {
        if (text.length >= 3) {
            var url = `/dashboard/query/news/${text}`
            axios.get(url)
                .then(function (response) {
                    this.state.results = response.data
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        }
        else {
            this.state.results = [];
        }


    },
    resultDisp: function (result) {
        return (
            <List key={result.pk} result={result}/>
        )

    },


    render: function () {

        return (
            <div>
                <form className="search dream-search" method="get" action="">
                    <input name="q" className="search" value={this.state.search} onChange={this.search}
                           placeholder="search articles"/>
                    <ul className="results">
                        {this.state.results.map(result => this.resultDisp(result))}
                    </ul>
                </form>

            </div>
        );
    }

});

module.exports = SearchAddForm;