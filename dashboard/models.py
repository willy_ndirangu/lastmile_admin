# Create your models here.
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from django.utils.http import urlquote
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from dashboard.pdf import receipts
from django.core.mail import EmailMessage, send_mass_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings
import requests
from django.db import connection


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        creates a user  with a given email and password
        """
        now = timezone.now()
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        name, domain = email.split('@')
        if domain != "lastmile-networks.net":
            raise ValueError("You must be na employee of Lastmile")
        user = self.model(email=email, is_staff=is_staff, is_superuser=is_superuser, is_active=True,
                          last_login=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class EmployeeLevel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, blank=True, null=True, unique=True)
    details = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("employee-levels")

    class Meta:
        db_table = 'employee_level'
        managed = True


class Employee(AbstractBaseUser, PermissionRequiredMixin):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45, blank=True, null=True)
    last_name = models.CharField(max_length=45, blank=True, null=True)
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, unique=True)
    title = models.CharField(max_length=65, blank=True, null=True)
    is_staff = models.BooleanField('staff status', default=False)
    is_active = models.BooleanField('active', default=True)
    is_superuser = models.BooleanField('superuser', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    employee_level = models.ForeignKey('EmployeeLevel', models.DO_NOTHING, default=1)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'phone_number', 'title']

    objects = CustomUserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        db_table = 'employee'

    def get_absolute_url(self):
        return "/dashboard/employee/%s/" % urlquote(self.email)

    def get_full_name(self):
        """
        returns the full name of a given user
        """
        full_name = '%s %s ' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return self.is_staff


class InternetPackageType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("internet-package-detail", kwargs={"pk": self.id})

    class Meta:
        db_table = 'internet_package_type'


class InternetPackageDetail(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    details = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    internet_package_type = models.ForeignKey('InternetPackageType', models.DO_NOTHING, null=True)

    def __str__(self):
        return self.internet_package_type.name + " " + self.name

    class Meta:
        db_table = 'internet_package_detail'

    def get_absolute_url(self):
        return reverse("packages-details-details", kwargs={"pk": self.id})


class Location(models.Model):
    id = models.AutoField(primary_key=True)
    location = models.CharField(max_length=45, unique=True)
    router_ip = models.CharField(max_length=16, default="41.215.69.82")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.location

    def get_absolute_url(self):
        return reverse("locations")

    class Meta:
        db_table = 'location'


class Payment(models.Model):
    id = models.AutoField(primary_key=True)
    amount = models.IntegerField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
    receipt_no = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    payment_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    email_receipt = models.BooleanField(default=False)
    subscriber = models.ForeignKey('Subscriber', models.DO_NOTHING, null=True)
    recorded_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True)
    updated_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True, related_name="updated_record")
    details = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.subscriber.email + "   " + self.amount

    def get_absolute_url(self):
        return reverse("payment-details", kwargs={"pk": self.id})

    class Meta:
        db_table = 'payment'


def pre_save_payment(sender, instance, *args, **kwargs):
    instance.receipt_no = receipts.gen_receipt_number()
    receipts.gen_receipt(instance.receipt_no, instance.subscriber, instance.amount, instance.details)
    if instance.subscriber is not None:
        cursor = connection.cursor()
        cursor.execute('SELECT api_token FROM users WHERE email LIKE %s', ['system@lastmile-networks.net'])
        row = cursor.fetchall()
        api_token = row[0][0]
        r = requests.get(
            'http://microtikapi.lastmile-networks.net/api/subscriber/unblock/' + instance.subscriber.subscriber_unique_key + '?api_token=' + api_token)
        if r.status_code is not 200:
            requests.get(
                'http://microtikapi.lastmile-networks.net/api/error/unblock/' + instance.subscriber.ip + "?api_token=" + api_token)

    if instance.email_receipt and instance.subscriber is not None:
        html_content = render_to_string('mails/receipts.html',
                                        {'body': instance.details})
        subject, from_email = ("Receipt", 'clientservice@lastmile-networks.net')

        msg = EmailMultiAlternatives(subject, html_content, from_email, [instance.subscriber.email],
                                     bcc=["team@lastmile-networks.net"])
        msg.content_subtype = "html"

        msg.attach_file(settings.MEDIA_ROOT + '/receipts/' + str(instance.receipt_no) + '.pdf')
        msg.send()


pre_save.connect(pre_save_payment, sender=Payment)


class Subscriber(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45, blank=True, null=True)
    last_name = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    mobile_number = models.CharField(max_length=10, blank=True, null=True)
    connection_end_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    location = models.ForeignKey('Location', models.DO_NOTHING, null=True)
    internet_package = models.ForeignKey('InternetPackageDetail', models.DO_NOTHING, null=True)
    connection_status = models.ForeignKey('ConnectionStatus', models.DO_NOTHING, null=True)
    created_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True)
    comments = models.TextField(blank=True, null=True)
    subscriber_unique_key = models.CharField(max_length=45, blank=True, null=True, unique=True)
    ip = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        db_table = 'subscriber'

    def get_absolute_url(self):
        return reverse("subscriber-detail", kwargs={"pk": self.id})


def pre_save_subscriber(sender, instance, *args, **kwargs):
    if instance.pk is None:
        now = timezone.now()
        if Subscriber.objects.all().count() == 0:
            subscriber_id = 1
        else:
            subscriber_id = Subscriber.objects.latest('id').id + 1
        unique_key = "LMN" + str(now.year) + str(now.month) + str(subscriber_id)
        instance.subscriber_unique_key = unique_key


pre_save.connect(pre_save_subscriber, sender=Subscriber)


class Tickets(models.Model):
    id = models.AutoField(primary_key=True)
    details = models.TextField(blank=True, null=True)
    completed_comment = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    due_date = models.DateTimeField(null=True)
    employee = models.ForeignKey('Employee', models.DO_NOTHING, null=True, related_name='assigned_employee')
    created_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True)
    updated_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True, related_name="updated_by_record")
    status = models.ForeignKey('TicketStatus', models.DO_NOTHING, null=True)
    priority = models.ForeignKey('Priority', models.DO_NOTHING, null=True)

    def __str__(self):
        return self.details

    def get_absolute_url(self):
        return reverse("ticket-details", kwargs={"pk": self.id})

    class Meta:
        db_table = 'tickets'


class TicketStatus(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=45, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.status

    class Meta:
        db_table = 'ticket_status'


class ConnectionStatus(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=45, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.status

    class Meta:
        db_table = 'connection_status'


class Priority(models.Model):
    id = models.AutoField(primary_key=True)
    priority = models.CharField(max_length=45, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.priority

    class Meta:
        db_table = 'priority'


class OtherPayments(models.Model):
    id = models.AutoField(primary_key=True)
    amount = models.IntegerField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
    receipt_no = models.IntegerField(blank=True, null=True, unique=True)  # Field name made lowercase.
    payment_date = models.DateTimeField(blank=True, null=True)
    to = models.CharField(max_length=1000, null=True, blank=True)
    payee_name = models.CharField(max_length=1000, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    email_receipt = models.BooleanField(default=False)
    recorded_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True)
    updated_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True,
                                   related_name="other_payments_updated_by_record")
    description = models.TextField(blank=False, null=False)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("other-payment-details", kwargs={"pk": self.id})

    class Meta:
        db_table = 'other_payments'


def pre_save_other_payment(sender, instance, *args, **kwargs):
    instance.receipt_no = receipts.gen_receipt_number()
    receipts.gen_receipt(instance.receipt_no, instance.payee_name, instance.amount, instance.description)

    if instance.email_receipt and instance.payee_name is not None:
        html_content = render_to_string('mails/receipts.html',
                                        {'body': instance.description})
        subject, from_email = ("Receipt", 'clientservice@lastmile-networks.net')

        msg = EmailMultiAlternatives(subject, html_content, from_email, [instance.to],
                                     bcc=["team@lastmile-networks.net"])
        msg.content_subtype = "html"

        msg.attach_file(settings.MEDIA_ROOT + '/receipts/' + str(instance.receipt_no) + '.pdf')
        msg.send()


pre_save.connect(pre_save_other_payment, sender=OtherPayments)


class IncomeStatement(models.Model):
    id = models.AutoField(primary_key=True)
    period = models.CharField(max_length=30, null=False)
    filename = models.TextField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Expenses(models.Model):
    id = models.AutoField(primary_key=True)
    amount = models.IntegerField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
    incurred_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    recorded_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True)
    updated_by = models.ForeignKey('Employee', models.DO_NOTHING, null=True,
                                   related_name="expenses_updated_by_record")
    description = models.TextField(blank=False, null=False)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("expenses-details", kwargs={"pk": self.id})

    class Meta:
        db_table = 'expense'
