import numpy as np
from django.template.loader import render_to_string
from weasyprint import HTML, CSS
from django.conf import settings
import num2words as nm
import dashboard.models


def gen_receipt_number():
    subscriber_payments_receipt_number = [payment.receipt_no for payment in dashboard.models.Payment.objects.all()]
    other_payments_receipt_number = [payment.receipt_no for payment in dashboard.models.OtherPayments.objects.all()]
    receipt_numbers = subscriber_payments_receipt_number + other_payments_receipt_number
    number = np.random.random_integers(1, 10000000)
    if number not in receipt_numbers:
        return number
    else:
        gen_receipt_number()


def gen_receipt(receipt_no, subscriber, amount, details):
    html = render_to_string('pdfs/receipts.html',
                            {'receipt_no': receipt_no, 'subscriber': subscriber, 'amount': amount, 'details': details,
                             'amount_in_words': nm.num2words(amount)})
    path = settings.MEDIA_ROOT + '/receipts/' + str(receipt_no) + '.pdf'
    pdf = HTML(string=html).write_pdf(target=path, stylesheets=[CSS(
        settings.STATIC_ROOT + '/css/pdfs.css')])

