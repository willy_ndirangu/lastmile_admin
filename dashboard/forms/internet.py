from dashboard.models import InternetPackageType, InternetPackageDetail
from django.forms import ModelForm, ModelChoiceField, Select
from dashboard.forms.forms import BaseForm


class PackageDetailForm(BaseForm):
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = InternetPackageDetail
        exclude = ['created_at', 'updated_at']

    def __init__(self, *args, **kwargs):
        super(PackageDetailForm, self).__init__(*args, **kwargs)
        self.fields['internet_package_type'] = ModelChoiceField(queryset=InternetPackageType.objects.all(),
                                                                widget=Select,
                                                                empty_label="Choose a a package")


class PackageTypeForm(BaseForm):
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = InternetPackageType
        exclude = ['created_at', 'updated_at']
