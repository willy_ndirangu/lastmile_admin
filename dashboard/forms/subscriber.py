from dashboard.models import Subscriber, Location, InternetPackageDetail, ConnectionStatus
from django.forms import ModelForm, ModelChoiceField, Select, DateTimeField


class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if "date" in field_name:
                field.widget.attrs['all-picker'] = ""
            field.widget.attrs['class'] = 'mdl-textfield__input subscriber-form'
            field.widget.attrs['required'] = True


class SubscriberForm(BaseForm):
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = Subscriber
        exclude = ['created_at', 'updated_at', 'subscriber_unique_key', 'created_by']

    def __init__(self, *args, **kwargs):
        super(SubscriberForm, self).__init__(*args, **kwargs)
        self.fields['location'] = ModelChoiceField(queryset=Location.objects.all(), widget=Select,
                                                   empty_label="Choose a location")
        self.fields['internet_package'] = ModelChoiceField(queryset=InternetPackageDetail.objects.all(),
                                                           widget=Select, empty_label="Choose a package")
        self.fields['connection_status'] = ModelChoiceField(queryset=ConnectionStatus.objects.all(),
                                                            widget=Select, empty_label="Choose connection status")
