from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import BaseUserManager
from dashboard.models import Employee, EmployeeLevel
from dashboard.forms.subscriber import BaseForm
from django import forms


class CustomUserCreationForm(UserCreationForm, BaseUserManager):
    """
    A form that creates a user given an email and password
    """
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Employee
        exclude = ['created_at', 'updated_at', 'last_login', 'password']

    def clean_email(self):
        email = self.cleaned_data['email']
        email = BaseUserManager.normalize_email(email)
        user, domain = email.split('@')
        if domain != 'lastmile-networks.net':
            raise forms.ValidationError("Only lastmile employees are allowed here")
        return email


class CustomUserChangeForm(forms.ModelForm):
    """
    A form that creates a user given an email and password
    """

    class Meta:
        model = Employee
        exclude = ['created_at', 'updated_at', 'last_login', 'is_superuser', 'password']


class PasswordChangingForm(PasswordChangeForm):
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'mdl-textfield__input'}))
    new_password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'mdl-textfield__input'}))
    new_password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'mdl-textfield__input'}))


class EmployeeLevelForm(forms.ModelForm):
    """
    A form that creates a user given an email and password
    """

    class Meta:
        model = EmployeeLevel
        exclude = ['created_at', 'updated_at']