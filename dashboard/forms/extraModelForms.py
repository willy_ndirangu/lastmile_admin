from dashboard.models import Location, Tickets, Payment, Employee, Subscriber, TicketStatus, Priority, OtherPayments, \
    Expenses
from django.forms import ModelForm, ModelChoiceField, Select
from dashboard.forms.forms import BaseForm
from tasks.models import Email


class LocationForm(BaseForm):
    error_css_class = 'mdl-color-text--red'

    class Meta:
        model = Location
        exclude = ['created_at', 'updated_at']


class TicketsForm(BaseForm):
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(TicketsForm, self).__init__(*args, **kwargs)
        self.fields['employee'] = ModelChoiceField(queryset=Employee.objects.all(), widget=Select,
                                                   empty_label="Choose an employee to assign")
        self.fields['status'] = ModelChoiceField(queryset=TicketStatus.objects.all(), widget=Select,
                                                 empty_label="Choose a status to assign")
        self.fields['priority'] = ModelChoiceField(queryset=Priority.objects.all(), widget=Select,
                                                   empty_label="Choose the tickets priority ")

        self.fields['status'].widget.attrs['ng-model'] = "status"

    class Meta:
        model = Tickets
        exclude = ['created_at', 'updated_at', 'updated_by', 'created_by', 'completed_comment']


class PaymentForm(ModelForm):
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.fields['subscriber'] = ModelChoiceField(queryset=Subscriber.objects.all(), widget=Select,
                                                     empty_label="Choose an subscriber whose payment belongs  to")
        for field_name, field in self.fields.items():
            if "date" in field_name:
                field.widget.attrs['all-picker'] = ""
        self.fields['details'].widget.attrs['ng-model'] = "details"
        self.fields['details'].widget.attrs['required'] = True
        self.fields['amount'].widget.attrs['required'] = True
        self.fields['payment_date'].widget.attrs['required'] = True
        self.fields['amount'].widget.attrs['ng-model'] = "amount"

    class Meta:
        model = Payment
        exclude = ['created_at', 'updated_at', 'recorded_by', 'updated_by', 'receipt_no']


class EmailForm(ModelForm):
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)
        self.fields['body'].widget.attrs['ng-model'] = "body"
        self.fields['subject'].widget.attrs['required'] = True
        self.fields['subject'].widget.attrs['ng-model'] = "subject"

    class Meta:
        model = Email
        exclude = ['created_at', 'updated_at', 'sent_by', 'to', 'from_email']


class OtherPaymentsForm(ModelForm):
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(OtherPaymentsForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if "date" in field_name:
                field.widget.attrs['all-picker'] = ""
        self.fields['description'].widget.attrs['ng-model'] = "description"
        self.fields['amount'].widget.attrs['required'] = True
        self.fields['payment_date'].widget.attrs['required'] = True
        self.fields['amount'].widget.attrs['ng-model'] = "amount"
        self.fields['email_receipt'].widget.attrs['ng-model'] = "email_receipt"

    class Meta:
        model = OtherPayments
        exclude = ['created_at', 'updated_at', 'recorded_by', 'updated_by', 'receipt_no', 'to']


class ExpensesForm(ModelForm):
    error_css_class = 'mdl-color-text--red'

    def __init__(self, *args, **kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if "date" in field_name:
                field.widget.attrs['all-picker'] = ""
        self.fields['description'].widget.attrs['ng-model'] = "description"
        self.fields['description'].widget.attrs['required'] = True
        self.fields['amount'].widget.attrs['required'] = True
        self.fields['incurred_date'].widget.attrs['required'] = True
        self.fields['amount'].widget.attrs['ng-model'] = "amount"

    class Meta:
        model = Expenses
        exclude = ['created_at', 'updated_at', 'recorded_by', 'updated_by']
