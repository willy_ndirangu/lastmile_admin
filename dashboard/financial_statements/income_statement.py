import numpy as np
from django.template.loader import render_to_string
from weasyprint import HTML, CSS
from django.conf import settings
import num2words as nm
import dashboard.models
from datetime import datetime
from django.utils.timezone import timedelta
from django.db.models import Sum


def gen_statement_pdf():
    current_month = (datetime.now() - timedelta(days=5)).month
    current_month_name = datetime.strftime(datetime.now() - timedelta(days=5), '%B')
    subscriber_payments = dashboard.models.Payment.objects.filter(payment_date__month=current_month).aggregate(
        Sum('amount'))
    other_payments = dashboard.models.OtherPayments.objects.filter(payment_date__month=current_month).aggregate(
        Sum('amount'))
    expenses = dashboard.models.Expenses.objects.filter(incurred_date__month=current_month)
    total_expense_amount = expenses.aggregate(Sum('amount'))
    html = render_to_string('pdfs/income_stmt.html',
                            {'subscriber_payments': subscriber_payments['amount__sum'],
                             'other_payments': other_payments['amount__sum'],
                             'total_costs': total_expense_amount['amount__sum'], 'expenses': expenses,
                             'month': current_month_name})
    filename = current_month_name
    path = settings.MEDIA_ROOT + '/financial-statements/' + filename + '.pdf'
    pdf = HTML(string=html).write_pdf(target=path, stylesheets=[CSS(
        settings.STATIC_ROOT + '/css/pdfs.css')])
    return filename
