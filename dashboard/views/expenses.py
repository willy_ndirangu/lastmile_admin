from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Expenses, Payment, OtherPayments, IncomeStatement
from dashboard.forms import extraModelForms
from dashboard.mixins import LoginRequiredMixin
from django.db import connection
from django.db.models import Sum, Count


class ExpensesListView(LoginRequiredMixin, ListView):
    model = Expenses
    queryset = Expenses.objects.order_by('-created_at')
    paginate_by = 9


class ExpensesDetailView(LoginRequiredMixin, DetailView):
    model = Expenses


class ExpensesCreateView(LoginRequiredMixin, CreateView):
    model = Expenses
    form_class = extraModelForms.ExpensesForm

    def form_valid(self, form):
        form.instance.recorded_by = self.request.user
        return super(ExpensesCreateView, self).form_valid(form)


class ExpensesUpdateView(LoginRequiredMixin, UpdateView):
    model = Expenses
    form_class = extraModelForms.ExpensesForm

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(ExpensesUpdateView, self).form_valid(form)


class FinancialStatementTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/financial_stmt.html'

    def get_context_data(self, **kwargs):
        context = super(FinancialStatementTemplateView, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['subscriber_payments'] = get_by_month_amount(Payment, 'payment_date', 'amount')
        context['other_payments'] = get_by_month_amount(OtherPayments, 'payment_date', 'amount')
        context['expenses'] = get_by_month_amount(Expenses, 'incurred_date', 'amount')
        context['income_statements'] = IncomeStatement.objects.all().order_by('-created_at')

        return context


def get_by_month_amount(model, date_field, summation_field):
    truncate_date = connection.ops.date_trunc_sql('month', date_field)
    qs = model.objects.extra({'month': truncate_date})
    report = qs.values('month').annotate(Sum(summation_field), Count('pk')).order_by('-month')
    return report
