from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Employee, EmployeeLevel
from dashboard.forms import forms
from dashboard.mixins import  LoginRequiredMixin


class EmployeeListView(LoginRequiredMixin, ListView):
    model = Employee
    paginate_by = 9


class EmployeeDetailView(LoginRequiredMixin, DetailView):
    model = Employee
    slug_field = 'email'


class EmployeeCreateView(LoginRequiredMixin, CreateView):
    model = Employee
    form_class = forms.CustomUserCreationForm


class EmployeeUpdateView(LoginRequiredMixin, UpdateView):
    model = Employee
    slug_field = 'email'
    form_class = forms.CustomUserChangeForm


class EmployeeLevelListView(LoginRequiredMixin, ListView):
    model = EmployeeLevel
    paginate_by = 9


class EmployeeLevelDetailView(LoginRequiredMixin, DetailView):
    model = EmployeeLevel


class EmployeeLevelCreateView(LoginRequiredMixin, CreateView):
    model = EmployeeLevel
    form_class = forms.EmployeeLevelForm


class EmployeeLevelUpdateView(LoginRequiredMixin, UpdateView):
    model = EmployeeLevel
    form_class = forms.EmployeeLevelForm
