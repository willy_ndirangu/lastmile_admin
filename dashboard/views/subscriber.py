from django.views.generic.list import ListView
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Subscriber, Location, InternetPackageDetail, InternetPackageType
from tasks.models import Email
from dashboard.forms import subscriber, extraModelForms
from django.db.models import Q
from dashboard.mixins import LoginRequiredMixin


class SubscriberListView(LoginRequiredMixin, ListView):
    model = Subscriber
    paginate_by = 9

    def get_context_data(self, **kwargs):
        context = super(SubscriberListView, self).get_context_data(**kwargs)
        context['location'] = Location.objects.all()
        return context

    def get_queryset(self, **kwargs):
        queryset = super(SubscriberListView, self).get_queryset(**kwargs)
        # Get the q GET parameter
        query = self.request.GET.get('q')
        if query:
            queryset = Subscriber.objects.all().filter(Q(first_name__icontains=query) |
                                                       Q(last_name__icontains=query) |
                                                       Q(location__location__icontains=query) |
                                                       Q(subscriber_unique_key__icontains=query) |
                                                       Q(internet_package__name__icontains=query) |
                                                       Q(email__icontains=query)
                                                       )

        return queryset


class SubscriberDetailView(LoginRequiredMixin, DetailView):
    model = Subscriber


class SubscriberCreateView(LoginRequiredMixin, CreateView):
    model = Subscriber
    form_class = subscriber.SubscriberForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(SubscriberCreateView, self).form_valid(form)


class SubscriberUpdateView(LoginRequiredMixin, UpdateView):
    model = Subscriber
    form_class = subscriber.SubscriberForm


class SubscriberRequestsTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/requests.html'

    def get_context_data(self, **kwargs):
        context = super(SubscriberRequestsTemplateView, self).get_context_data(**kwargs)
        context['request'] = self.request
        return context


class SubscriberEmailCreateView(LoginRequiredMixin, CreateView):
    model = Email
    form_class = extraModelForms.EmailForm
    template_name = 'dashboard/emails.html'

    def form_valid(self, form):
        form.instance.sent_by = self.request.user
        form.instance.to = self.request.POST.get('to', '')
        form.instance.from_email = self.request.POST.get('from_email', '')
        return super(SubscriberEmailCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SubscriberEmailCreateView, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['locations'] = Location.objects.all()

        return context


class SubscriberEmailListView(LoginRequiredMixin, ListView):
    template_name = 'dashboard/sent_emails.html'
    model = Email
    paginate_by = 9


class SubscriberEmailDetailView(LoginRequiredMixin, DetailView):
    template_name = 'dashboard/sent_email_detail.html'
    model = Email


class SubscriberReportTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'reports/subscriber_report.html'

    def get_context_data(self, **kwargs):
        context = super(SubscriberReportTemplateView, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['locations'] = Location.objects.all()
        context['packages'] = InternetPackageDetail.objects.all()
        context['packages_types'] = InternetPackageType.objects.all()

        return context
