from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Location
from dashboard.forms import extraModelForms
from dashboard.mixins import LoginRequiredMixin


class LocationListView(LoginRequiredMixin, ListView):
    model = Location
    paginate_by = 9


class LocationDetailView(LoginRequiredMixin, DetailView):
    model = Location


class LocationCreateView(LoginRequiredMixin, CreateView):
    model = Location
    form_class = extraModelForms.LocationForm


class LocationUpdateView(LoginRequiredMixin, UpdateView):
    model = Location
    form_class = extraModelForms.LocationForm
