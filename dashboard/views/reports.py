from dashboard.models import Subscriber, Payment, OtherPayments, Expenses
from rest_framework.generics import ListAPIView
from api.serializers.serializers import SubscriberSerializer, Payment
from dashboard.mixins import LoginRequiredMixin
from dashboard.views.expenses import get_by_month_amount


class RequestListApiView(LoginRequiredMixin, ListAPIView):
    queryset = Subscriber.objects.select_related('location').all()
    serializer_class = SubscriberSerializer


class RequestPaymentDataApiView(LoginRequiredMixin, ListAPIView):
    queryset = Payment.objects.select_related('location').all()
    serializer_class = SubscriberSerializer
