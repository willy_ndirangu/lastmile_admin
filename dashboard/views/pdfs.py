from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from dashboard.models import Subscriber, Tickets
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils.timezone import now
import weasyprint
from django.core.files.storage import FileSystemStorage


@login_required
def subscriber_pdf(request):
    subscribers = Subscriber.objects.all()
    location = request.POST.get('location', "")
    package = request.POST.get('package', "")
    package_type = request.POST.get('package_type', "")
    if location:
        subscribers = subscribers.filter(location__location=location)
    if package_type:
        subscribers = subscribers.filter(internet_package__internet_package_type__name=package_type)
    if package:
        subscribers = subscribers.filter(internet_package__name=package)

    html = render_to_string('pdfs/subscriber_report.html',
                            {'subscribers': subscribers})
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=\
            "subscriber_report_{}.pdf"'.format(now().strftime('%D-%H:%M:%S'))
    weasyprint.HTML(string=html).write_pdf(response,
                                           stylesheets=[weasyprint.CSS(
                                               settings.STATIC_ROOT + '/css/pdfs.css')])
    return response


@login_required
def receipt_pdf(request, receipt_number):
    fs = FileSystemStorage(settings.MEDIA_ROOT + '/receipts/')
    with fs.open(str(receipt_number) + '.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename= "{}.pdf"'.format(receipt_number)
        return response

    return response


@login_required
def income_statement_pdf(request, filename):
    fs = FileSystemStorage(settings.MEDIA_ROOT + '/financial-statements/')
    with fs.open(str(filename) + '.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename= "{}.pdf"'.format(filename)
        return response

    return response
