from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import OtherPayments
from dashboard.forms import extraModelForms
from dashboard.mixins import LoginRequiredMixin


class OtherPaymentsListView(LoginRequiredMixin, ListView):
    model = OtherPayments
    queryset = OtherPayments.objects.order_by('-created_at')
    paginate_by = 9


class OtherPaymentsDetailView(LoginRequiredMixin, DetailView):
    model = OtherPayments


class OtherPaymentsCreateView(LoginRequiredMixin, CreateView):
    model = OtherPayments
    form_class = extraModelForms.OtherPaymentsForm

    def form_valid(self, form):
        form.instance.recorded_by = self.request.user
        form.instance.to = self.request.POST.get('to', '')
        return super(OtherPaymentsCreateView, self).form_valid(form)


class OtherPaymentsUpdateView(LoginRequiredMixin, UpdateView):
    model = OtherPayments
    form_class = extraModelForms.OtherPaymentsForm

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(OtherPaymentsUpdateView, self).form_valid(form)
