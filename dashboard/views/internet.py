from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import InternetPackageType, InternetPackageDetail
from dashboard.forms import internet
from dashboard.mixins import LoginRequiredMixin


class InternetPackageDetailListView(LoginRequiredMixin, ListView):
    model = InternetPackageDetail
    paginate_by = 9


class InternetPackageDetailView(LoginRequiredMixin, DetailView):
    model = InternetPackageDetail


class InternetPackageDetailCreateView(LoginRequiredMixin, CreateView):
    model = InternetPackageDetail
    form_class = internet.PackageDetailForm


class InternetPackageDetailUpdateView(LoginRequiredMixin, UpdateView):
    model = InternetPackageDetail
    form_class = internet.PackageDetailForm


class InternetPackageTypeListView(LoginRequiredMixin, ListView):
    model = InternetPackageType
    paginate_by = 9


class InternetPackageTypeView(LoginRequiredMixin, DetailView):
    model = InternetPackageType


class InternetPackageTypeCreateView(LoginRequiredMixin, CreateView):
    model = InternetPackageType
    form_class = internet.PackageTypeForm


class InternetPackageTypeUpdateView(LoginRequiredMixin, UpdateView):
    model = InternetPackageType
    form_class = internet.PackageTypeForm

