from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from tasks.models import Email
from dashboard.forms import forms
from dashboard.mixins import LoginRequiredMixin


class EmailListView(LoginRequiredMixin, ListView):
    model = Email
    paginate_by = 9


class EmailDetailView(LoginRequiredMixin, DetailView):
    model = Email
    slug_field = 'id'


class EmailCreateView(LoginRequiredMixin, CreateView):
    model = Email
    form_class = forms.CustomUserCreationForm
