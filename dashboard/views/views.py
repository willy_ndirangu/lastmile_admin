from django.shortcuts import render, redirect
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from dashboard.models import Subscriber, Tickets


# Create your views here.


def home(request):
    # ...

    # Return a "created" (201) response code.
    if not request.user.is_authenticated():
        return render(request, 'auth/login.html')
    return HttpResponseRedirect('/dashboard')


def authenticate(request):
    """
    Authenticates a user given username and password
    """

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    next_page = request.POST.get('next_page', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        if user.last_login is None:
            auth.login(request, user)
            return HttpResponseRedirect('/change_password')
        else:
            auth.login(request, user)
            if next_page == "":
                return HttpResponseRedirect('/dashboard')
            else:
                return HttpResponseRedirect(next_page)

    else:
        messages.add_message(request, messages.ERROR, "The credentials don't match our records")
        return HttpResponseRedirect('/')


def logout_view(request):
    """
    logout a user and redirect to homepage
    :param request:
    :return:
    """
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def dashboard(request):
    try:
        latest_subscriber = Subscriber.objects.latest('created_at')
    except Subscriber.DoesNotExist:
        latest_subscriber = None
    try:
        latest_ticket = Tickets.objects.filter(status__status="pending").latest('created_at')
    except Tickets.DoesNotExist:
        latest_ticket = None

    return render(request, template_name="layouts/dashboard.html",
                  context={'subscriber': latest_subscriber, 'ticket': latest_ticket})
