from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Tickets
from dashboard.forms import extraModelForms
from dashboard.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class TicketsListView(LoginRequiredMixin, ListView):
    model = Tickets
    queryset = Tickets.objects.exclude(status__status='completed').order_by('-id')
    paginate_by = 9


class TicketsDetailView(LoginRequiredMixin, DetailView):
    model = Tickets


class TicketsCreateView(LoginRequiredMixin, CreateView):
    model = Tickets
    form_class = extraModelForms.TicketsForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.completed_comment = self.request.POST.get('completed_comment', '')
        return super(TicketsCreateView, self).form_valid(form)


class TicketsUpdateView(LoginRequiredMixin, UpdateView):
    model = Tickets
    form_class = extraModelForms.TicketsForm

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        form.instance.completed_comment = self.request.POST.get('completed_comment', '')
        return super(TicketsUpdateView, self).form_valid(form)


class CompletedTicketsTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/completed_tickets.html'

    def get_context_data(self, **kwargs):
        context = super(CompletedTicketsTemplateView, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['tickets'] = Tickets.objects.filter(status__status='completed').order_by('-id')
        return context
