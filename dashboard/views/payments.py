from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from dashboard.models import Payment
from dashboard.forms import extraModelForms
from dashboard.mixins import LoginRequiredMixin


class PaymentListView(LoginRequiredMixin, ListView):
    model = Payment
    queryset = Payment.objects.order_by('-created_at')
    paginate_by = 9


class PaymentDetailView(LoginRequiredMixin, DetailView):
    model = Payment


class PaymentCreateView(LoginRequiredMixin, CreateView):
    model = Payment
    form_class = extraModelForms.PaymentForm

    def form_valid(self, form):
        form.instance.recorded_by = self.request.user
        return super(PaymentCreateView, self).form_valid(form)


class PaymentUpdateView(LoginRequiredMixin, UpdateView):
    model = Payment
    form_class = extraModelForms.PaymentForm

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PaymentUpdateView, self).form_valid(form)
