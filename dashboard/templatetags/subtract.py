from django import template
from datetime import datetime
from django.utils.timezone import timedelta

register = template.Library()


@register.filter(name='sub')
def subtract(value, arg):
    return value - arg


@register.filter(name='month_year')
def month_year(date):
    return datetime.strftime(date, '%B') + "   " + str(date.year)


@register.simple_tag
def add_sub_total(quote_data):
    subtotal = 0
    for item in quote_data:
        subtotal += item.quantity * item.unit_price
    return format(float(subtotal), '.2f')


@register.simple_tag
def vat(quote_data):
    subtotal = 0
    for item in quote_data:
        subtotal += item.quantity * item.unit_price
    return format(float(subtotal * 0.16), '.2f')


@register.simple_tag
def vat_plus_total(quote_data):
    subtotal = 0
    for item in quote_data:
        subtotal += item.quantity * item.unit_price
    return format(float(subtotal * 0.16 + subtotal), '.2f')


@register.simple_tag
def multiply_two_nums(a, b):
    return format(float(a * b), '.2f')


@register.filter(name='add_month')
def subtract(date):
    return date + timedelta(days=30)
