"""lastmile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth.views import password_change
from dashboard.views import views, subscriber, employee, internet, location, tickets, payments, reports, pdfs, \
    other_payments, expenses
from dashboard.forms import forms

urlpatterns = [
    url(r'^$', views.home, name='index'),
    url(r'^account/login', views.authenticate, name='authenticate'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^change_password/$', password_change,
        {'template_name': 'auth/change_password.html', 'password_change_form': forms.PasswordChangingForm,
         'post_change_redirect': views.dashboard}, name="password_change"),
    url(r'^dashboard/subscribers$', subscriber.SubscriberListView.as_view(), name='subscribers'),
    url(r'dashboard/subscriber/(?P<pk>[-\w]+)/$', subscriber.SubscriberDetailView.as_view(), name='subscriber-detail'),
    url(r'dashboard/subscriber/(?P<pk>[-\w]+)/update/$', subscriber.SubscriberUpdateView.as_view(),
        name='subscriber-update'),
    url(r'dashboard/subscribers/add/$', subscriber.SubscriberCreateView.as_view(), name='subscriber-add'),
    url(r'^dashboard/employees$', employee.EmployeeListView.as_view(), name='employees'),
    url(r'dashboard/employee/(?P<slug>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$',
        employee.EmployeeDetailView.as_view(), name='employee-detail'),
    url(r'dashboard/employee/(?P<slug>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/update/$',
        employee.EmployeeUpdateView.as_view(),
        name='employee-update'),
    url(r'dashboard/employees/add/$', employee.EmployeeCreateView.as_view(), name='employee-add'),

    # internet packages urls

    url(r'^dashboard/internet/packages/$', internet.InternetPackageTypeListView.as_view(),
        name='internet-package-types'),
    url(r'dashboard/internet/packages/(?P<pk>[-\w]+)/$',
        internet.InternetPackageTypeView.as_view(), name='internet-package-detail'),
    url(r'dashboard/internet/package/(?P<pk>[-\w]+)/update/$',
        internet.InternetPackageTypeUpdateView.as_view(),
        name='internet-package-update'),
    url(r'dashboard/internet/packages/add/new/$', internet.InternetPackageTypeCreateView.as_view(),
        name='internet-package-add'),

    # internet packages details

    url(r'^dashboard/internet/packages/details/all/$', internet.InternetPackageDetailListView.as_view(),
        name='packages-details'),
    url(r'dashboard/internet/packages/detail/(?P<pk>[-\w]+)/$',
        internet.InternetPackageDetailView.as_view(), name='packages-details-details'),
    url(r'dashboard/internet/packages/detail/(?P<pk>[-\w]+)/update/$',
        internet.InternetPackageDetailUpdateView.as_view(),
        name='packages-details-update'),
    url(r'dashboard/internet/packages/details/add/$', internet.InternetPackageDetailCreateView.as_view(),
        name='packages-details-add'),

    # Employee level

    url(r'^dashboard/employee/levels/$', employee.EmployeeLevelListView.as_view(),
        name='employee-levels'),
    url(r'dashboard/employee/level/detail/(?P<pk>[-\w]+)/$',
        employee.EmployeeLevelDetailView.as_view(), name='employee-level-details'),
    url(r'dashboard/employee/level/(?P<pk>[-\w]+)/update/$',
        employee.EmployeeLevelUpdateView.as_view(),
        name='employee-level-update'),
    url(r'dashboard/employee/level/add/$', employee.EmployeeLevelCreateView.as_view(),
        name='employee-level-add'),

    # Location

    url(r'^dashboard/locations/all/$', location.LocationListView.as_view(),
        name='locations'),
    url(r'dashboard/location/detail/(?P<pk>[-\w]+)/$',
        location.LocationDetailView.as_view(), name='location-details'),
    url(r'dashboard/location/(?P<pk>[-\w]+)/update/$',
        location.LocationUpdateView.as_view(),
        name='location-update'),
    url(r'dashboard/locations/add/$', location.LocationCreateView.as_view(),
        name='location-add'),

    # Tickets

    url(r'^dashboard/tickets/all/$', tickets.TicketsListView.as_view(),
        name='tickets'),
    url(r'dashboard/tickets/detail/(?P<pk>[-\w]+)/$',
        tickets.TicketsDetailView.as_view(), name='ticket-details'),
    url(r'dashboard/ticket/(?P<pk>[-\w]+)/update/$',
        tickets.TicketsUpdateView.as_view(),
        name='ticket-update'),
    url(r'dashboard/tickets/add/$', tickets.TicketsCreateView.as_view(),
        name='ticket-add'),
    url(r'dashboard/tickets/completed/$', tickets.CompletedTicketsTemplateView.as_view(),
        name='completed-tickets'),

    # Payments

    url(r'^dashboard/payments/all/$', payments.PaymentListView.as_view(),
        name='payments'),
    url(r'dashboard/payments/detail/(?P<pk>[-\w]+)/$',
        payments.PaymentDetailView.as_view(), name='payment-details'),
    url(r'dashboard/payment/(?P<pk>[-\w]+)/update/$',
        payments.PaymentUpdateView.as_view(),
        name='payment-update'),
    url(r'dashboard/payments/add/$', payments.PaymentCreateView.as_view(),
        name='payment-add'),

    # Requests
    url(r'^dashboard/requests$', subscriber.SubscriberRequestsTemplateView.as_view(),
        name='internet-requests'),
    url(r'^dashboard/emails$', subscriber.SubscriberEmailCreateView.as_view(),
        name='emails'),
    url(r'^dashboard/sent/emails$', subscriber.SubscriberEmailListView.as_view(),
        name='emails-sent'),
    url(r'^dashboard/view/email/(?P<pk>[-\w]+)/sent$', subscriber.SubscriberEmailDetailView.as_view(),
        name='email-detail'),
    url(r'^dashboard/reports/subscriber$', subscriber.SubscriberReportTemplateView.as_view(),
        name='customer-report'),

    # reports
    url(r'^dashboard/reports/subscriber/all$', reports.RequestListApiView.as_view(),
        name='all-subscribers'),

    url(r'^dashboard/reports/subscribers/pdf$', pdfs.subscriber_pdf, name='pdf-subscribers'),
    url(r'^dashboard/receipts/(?P<receipt_number>\d+)$', pdfs.receipt_pdf, name='pdf-receipt'),

    # other payments
    # Payments

    url(r'^dashboard/other/payments/all/$', other_payments.OtherPaymentsListView.as_view(),
        name='other-payments'),
    url(r'dashboard/other/payments/detail/(?P<pk>[-\w]+)/$',
        other_payments.OtherPaymentsDetailView.as_view(), name='other-payment-details'),
    url(r'dashboard/other/payment/(?P<pk>[-\w]+)/update/$',
        other_payments.OtherPaymentsUpdateView.as_view(),
        name='other-payment-update'),
    url(r'dashboard/other/payments/add/$', other_payments.OtherPaymentsCreateView.as_view(),
        name='other-payment-add'),

    # Expenses

    url(r'^dashboard/expenses/all/$', expenses.ExpensesListView.as_view(),
        name='expenses'),
    url(r'dashboard/expenses/detail/(?P<pk>[-\w]+)/$',
        expenses.ExpensesDetailView.as_view(), name='expenses-details'),
    url(r'dashboard/expenses/(?P<pk>[-\w]+)/update/$',
        expenses.ExpensesUpdateView.as_view(),
        name='expenses-update'),
    url(r'dashboard/expenses/add/$', expenses.ExpensesCreateView.as_view(),
        name='expenses-add'),

    # Financial statements
    url(r'dashboard/financial/statements/$', expenses.FinancialStatementTemplateView.as_view(),
        name='financial-stmt'),
    url(r'^dashboard/income/(?P<filename>[-\w]+)/statement$', pdfs.income_statement_pdf, name='income-statement'),

]
