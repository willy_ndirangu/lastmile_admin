from .models import Employee


class CustomUserAuth(object):

    def authenticate(self, username=None, password=None):
        try:
            user = Employee.objects.get(email=username)
            if user.check_password(password):
                return user
        except Employee.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            user = Employee.objects.get(pk=user_id)

            if user.is_active:
                return user
            return None
        except Employee.DoesNotExist:
            return None


